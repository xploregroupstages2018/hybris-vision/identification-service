package eu.elision.hybrisvision.hybrisvisionidentificationservice;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ApplicationStartTest {

    @Test
    public void applicationStarts() {
        HybrisvisionIdentificationServiceApplication.main(new String[]{});
        Assert.assertNotNull(HybrisvisionIdentificationServiceApplication.class);
    }
}