package eu.elision.hybrisvision.hybrisvisionidentificationservice.service;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.exception.ImageProcessingException;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.model.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
public class ImageServiceTest {

    private static final String TEST_IMAGE_NAME = "324617-Nutella-750g.jpg";

    @Autowired
    private ImageService imageService;

    @TestConfiguration
    static class ImageServiceTestContextConfiguration {

        @Bean
        public ImageService imageService() {
            return new ImageService();
        }
    }

    @Test(expected = ImageProcessingException.class)
    public void convertToBytes_emptyFile_throwError() throws Exception {

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", new byte[0]);
        imageService.convertToBytes(mockMultipartFile);
    }

    @Test
    public void convertToBytes_validFile_validBytes() throws Exception {

        ClassLoader classLoader = this.getClass().getClassLoader();
        File initialFile = new File(classLoader.getResource(TEST_IMAGE_NAME).getFile());

        InputStream targetStream = new FileInputStream(initialFile);
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", targetStream);

        byte[] bytes = imageService.convertToBytes(mockMultipartFile);
        assertNotNull(bytes);
    }


    @Test
    public void getLabels_validAnnotatedImageResponse_labels() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        Response labels = imageService.getLabels(annotateImageResponse);
        assertNotNull(labels);
    }

    @Test
    public void getLabels_null_null() {

        Response labels = imageService.getLabels(null);
        assertNull(labels);
    }
}