package eu.elision.hybrisvision.hybrisvisionidentificationservice.util;

import com.google.cloud.vision.v1.*;
import com.google.type.Color;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ImageUtilTest {

    @Test
    public void getColors_imageWithoutColors_emptyList() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        List<String> colors = ImageUtil.getColors(annotateImageResponse);
        assertEquals(0, colors.size());
    }

    @Test
    public void getColors_imageWithColors_listWithItems() {

        ColorInfo colorInfo = ColorInfo.newBuilder()
                .setColor(Color.newBuilder().setBlue(10).build())
                .build();

        DominantColorsAnnotation dominantColorsAnnotation = DominantColorsAnnotation.newBuilder()
                .addColors(colorInfo)
                .build();

        ImageProperties imageProperties = ImageProperties.newBuilder()
                .setDominantColors(dominantColorsAnnotation)
                .build();

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.newBuilder()
                .setImagePropertiesAnnotation(imageProperties)
                .build();

        List<String> colors = ImageUtil.getColors(annotateImageResponse);
        assertEquals(1, colors.size());
    }

    @Test
    public void getWebSearch_imageWithoutWebSearch_emptyList() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        Map<String, Double> webSearch = ImageUtil.getWebSearch(annotateImageResponse);
        assertEquals(0, webSearch.size());
    }

    @Test
    public void getWebSearch_imageWithWebSearch_listWithValidDescription() {

        WebDetection.WebEntity webEntity1 = WebDetection.WebEntity.newBuilder()
                .setDescription("This is a test description")
                .setScore(1.0f)
                .build();

        WebDetection.WebEntity webEntity2 = WebDetection.WebEntity.newBuilder()
                .setDescription("This is another test description")
                .setScore(2.0f)
                .build();

        WebDetection webDetection = WebDetection.newBuilder()
                .addWebEntities(0, webEntity1)
                .addWebEntities(1, webEntity2)
                .build();

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.newBuilder()
                .setWebDetection(webDetection)
                .build();

        Map<String, Double> webSearch = ImageUtil.getWebSearch(annotateImageResponse);
        assertEquals(2, webSearch.size());
        assertEquals(1.0f, webSearch.get("This is a test description"), 0);
        assertEquals(2.0f, webSearch.get("This is another test description"), 0);
    }


    @Test
    public void extractSafeSearch_imageWithoutSafeSearch_unknownValuesInMap() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        Map<String, String> safeSearchMap = ImageUtil.extractSafeSearch(annotateImageResponse);

        String adultValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_ADULT);
        String spoofValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_SPOOF);
        String medicalValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_MEDICAL);
        String violenceValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_VIOLENCE);
        String racyValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_RACY);

        assertEquals(5, safeSearchMap.size());
        assertEquals(Likelihood.UNKNOWN.name(), adultValue);
        assertEquals(Likelihood.UNKNOWN.name(), spoofValue);
        assertEquals(Likelihood.UNKNOWN.name(), medicalValue);
        assertEquals(Likelihood.UNKNOWN.name(), violenceValue);
        assertEquals(Likelihood.UNKNOWN.name(), racyValue);
    }

    @Test
    public void extractSafeSearch_imageWithSafeSearch_knownValuesInMap() {

        SafeSearchAnnotation safeSearchAnnotation = SafeSearchAnnotation.newBuilder()
                .setAdult(Likelihood.VERY_UNLIKELY)
                .setSpoof(Likelihood.UNLIKELY)
                .setMedical(Likelihood.POSSIBLE)
                .setViolence(Likelihood.LIKELY)
                .setRacy(Likelihood.VERY_LIKELY)
                .build();

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.newBuilder()
                .setSafeSearchAnnotation(safeSearchAnnotation)
                .build();

        Map<String, String> safeSearchMap = ImageUtil.extractSafeSearch(annotateImageResponse);

        String adultValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_ADULT);
        String spoofValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_SPOOF);
        String medicalValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_MEDICAL);
        String violenceValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_VIOLENCE);
        String racyValue = safeSearchMap.get(ImageUtil.SAFE_SEARCH_RACY);

        assertEquals(5, safeSearchMap.size());
        assertEquals(Likelihood.VERY_UNLIKELY.name(), adultValue);
        assertEquals(Likelihood.UNLIKELY.name(), spoofValue);
        assertEquals(Likelihood.POSSIBLE.name(), medicalValue);
        assertEquals(Likelihood.LIKELY.name(), violenceValue);
        assertEquals(Likelihood.VERY_LIKELY.name(), racyValue);
    }

    @Test
    public void extractText_imageWithoutText_emptyList() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        List<String> text = ImageUtil.extractText(annotateImageResponse);
        assertEquals(0, text.size());
    }

    @Test
    public void extractText_imageWith_listWithText() {

        EntityAnnotation entityAnnotation1 = EntityAnnotation.newBuilder()
                .setDescription("This is text")
                .build();

        EntityAnnotation entityAnnotation2 = EntityAnnotation.newBuilder()
                .setDescription("This is also text")
                .build();

        EntityAnnotation entityAnnotation3 = EntityAnnotation.newBuilder()
                .setDescription("This should be filtered\n")
                .build();

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.newBuilder()
                .addTextAnnotations(0, entityAnnotation1)
                .addTextAnnotations(1, entityAnnotation2)
                .addTextAnnotations(2, entityAnnotation3)
                .build();

        List<String> text = ImageUtil.extractText(annotateImageResponse);
        assertEquals(2, text.size());
        assertEquals("This is also text", text.get(1));
    }

    @Test
    public void extractLabels_imageWithoutLabels_emptyList() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        Map<String, Double> labels = ImageUtil.extractLabels(annotateImageResponse);
        assertEquals(0, labels.size());
    }

    @Test
    public void extractLabels_imageWithLabels_listWithLabels() {

        EntityAnnotation entityAnnotation1 = EntityAnnotation.newBuilder()
                .setDescription("This is text")
                .setScore(0.0f)
                .build();

        EntityAnnotation entityAnnotation2 = EntityAnnotation.newBuilder()
                .setDescription("This is also text")
                .setScore(2.0f)
                .build();

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.newBuilder()
                .addLabelAnnotations(0, entityAnnotation1)
                .addLabelAnnotations(1, entityAnnotation2)
                .build();

        Map<String, Double> labels = ImageUtil.extractLabels(annotateImageResponse);
        assertEquals(2, labels.size());
        assertEquals(0.0f, labels.get("This is text").floatValue(), 0);
        assertEquals(2.0f, labels.get("This is also text").floatValue(), 0);
    }

    @Test
    public void getLogos_imageWithoutLogos_emptyList() {

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.getDefaultInstance();
        List<String> logos = ImageUtil.getLogos(annotateImageResponse);
        assertEquals(0, logos.size());
    }

    @Test
    public void getLogos_imageWithLogos_listWithLogos() {

        EntityAnnotation entityAnnotation1 = EntityAnnotation.newBuilder()
                .setDescription("This is a logo description")
                .build();

        EntityAnnotation entityAnnotation2 = EntityAnnotation.newBuilder()
                .setDescription("This is also a logo description")
                .build();

        AnnotateImageResponse annotateImageResponse = AnnotateImageResponse.newBuilder()
                .addLogoAnnotations(0, entityAnnotation1)
                .addLogoAnnotations(1, entityAnnotation2)
                .build();

        List<String> logos = ImageUtil.getLogos(annotateImageResponse);
        assertEquals(2, logos.size());
        assertEquals("This is also a logo description", logos.get(1));
    }
}