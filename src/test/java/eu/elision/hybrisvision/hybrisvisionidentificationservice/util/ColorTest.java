package eu.elision.hybrisvision.hybrisvisionidentificationservice.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * @author Lander
 * @since 9/05/2018
 */

public class ColorTest {

    @MethodSource
    @ParameterizedTest(name = "rgb({0}, {1}, {2}) should return {3}")
    public void getName(ArgumentsAccessor args) {
        assertThat(Color.of(args.getInteger(0), args.getInteger(1), args.getInteger(2)).getName())
                .isEqualTo(args.getString(3));
    }

    public static Stream<Arguments> getName() {
        return Stream.of(
                Arguments.of(255, 0, 0, "Red"),
                Arguments.of(200, 0, 0, "Red"),
                Arguments.of(0, 255, 0, "Green"),
                Arguments.of(0, 0, 255, "Blue"),
                Arguments.of(255, 255, 255, "White"),
                Arguments.of(0, 0, 0, "Black"),
                Arguments.of(255, 255, 0, "Yellow"),
                Arguments.of(128, 128, 128, "Gray"),
                Arguments.of(255, 165, 0, "Orange"));
    }
}

