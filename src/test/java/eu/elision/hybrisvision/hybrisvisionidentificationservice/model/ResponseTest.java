package eu.elision.hybrisvision.hybrisvisionidentificationservice.model;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class ResponseTest {

    @Test
    public void createResponse() {

        Map<String, String> map = new HashMap<>();
        map.put("key", "value");

        Response response = new Response.ResponseBuilder()
                .withColors(Arrays.asList("color1"))
                .withLabels(Collections.singletonMap("label1", 1.0))
                .withLogos(Collections.singletonList("logo1"))
                .withText(Arrays.asList("text1"))
                .withWeb(Collections.singletonMap("web1", 1.0))
                .withSafeSearch(map)
                .build();

        List<String> colors = response.getColors();
        Map<String, Double> labels = response.getLabels();
        List<String> logos = response.getLogos();
        List<String> textList = response.getTextList();
        Map<String, Double> webSearch = response.getWebSearch();
        Map<String, String> safeSearch = response.getSafeSearch();

        assertEquals("color1", colors.get(0));
        assertEquals(1.0f, labels.get("label1"), 0);
        assertEquals("logo1", logos.get(0));
        assertEquals("text1", textList.get(0));
        assertEquals(1.0f, webSearch.get("web1"), 0);
        assertEquals("value", safeSearch.get("key"));
    }
}