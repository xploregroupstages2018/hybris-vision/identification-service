package eu.elision.hybrisvision.hybrisvisionidentificationservice.controller;

import com.google.gson.Gson;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.exception.ImageProcessingException;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.facade.IdentificationFacade;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.model.Response;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * More info on testing can be found here: http://www.baeldung.com/spring-boot-testing
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ImageController.class)
public class ImageControllerTest {

    private static final String TEST_IMAGE_NAME = "324617-Nutella-750g.jpg";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IdentificationFacade identificationFacade;

    @Test
    public void postImage_success() throws Exception {

        ClassLoader classLoader = this.getClass().getClassLoader();
        Response response = getJsonResponse(classLoader);
        File initialFile = new File(classLoader.getResource(TEST_IMAGE_NAME).getFile());

        InputStream targetStream = new FileInputStream(initialFile);
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", targetStream);
        given(identificationFacade.identifyImage(mockMultipartFile)).willReturn(response);

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders
                        .multipart("/image")
                        .file(mockMultipartFile))
                .andExpect(status().is(200))
                .andExpect(jsonPath("logos").value("Nutella"))
                .andReturn();

        assertNotNull(mvcResult);
    }

    @Test
    public void postImage_failure() throws Exception {

        ClassLoader classLoader = this.getClass().getClassLoader();
        File initialFile = new File(classLoader.getResource(TEST_IMAGE_NAME).getFile());

        InputStream targetStream = new FileInputStream(initialFile);
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", targetStream);

        given(identificationFacade.identifyImage(mockMultipartFile)).willThrow(ImageProcessingException.class);

        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders
                        .multipart("/image")
                        .file(mockMultipartFile))
                .andExpect(status().is(500))
                .andExpect(status().reason(containsString("Something went wrong while processing the image")))
                .andReturn();

        assertNotNull(mvcResult);
    }

    private Response getJsonResponse(ClassLoader classLoader) throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();
        File jsonResponse = new File(classLoader.getResource("response.json").getFile());
        String s = jsonParser.parse(new FileReader(jsonResponse)).toString();
        return new Gson().fromJson(s, Response.class);
    }
}