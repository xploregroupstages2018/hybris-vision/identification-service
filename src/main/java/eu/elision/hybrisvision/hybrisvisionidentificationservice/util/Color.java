package eu.elision.hybrisvision.hybrisvisionidentificationservice.util;

/**
 * @author Lander
 * @since 9/05/2018
 */

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BinaryOperator;

public class Color {
    private static final Color[] KNOWN_COLORS = {
            Color.of("Beige", 245, 245, 220),
            Color.of("Black", 0, 0, 0),
            Color.of("Blue", 0, 0, 255),
            Color.of("Brown", 165, 42, 42),
            Color.of("Gray", 128, 128, 128),
            Color.of("Green", 0, 128, 0),
            Color.of("Orange", 255, 165, 0),
            Color.of("Pink", 255, 192, 203),
            Color.of("Purple", 128, 0, 128),
            Color.of("Red", 255, 0, 0),
            Color.of("White", 255, 255, 255),
            Color.of("Yellow", 255, 255, 0),
            Color.of("Petrol", 22, 115, 143),
            Color.of("Turquoise", 64, 224, 208),
            Color.of("Olive", 128, 128, 0),
            Color.of("Gold", 255, 215, 0),
            Color.of("Silver", 192, 192, 192)
    };

    private final String name;
    private final int r;
    private final int g;
    private final int b;

    private Color(String name, int r, int g, int b) {
        this.name = name;
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public String getName() {
        return name;
    }

    private int mse(int r, int g, int b) {
        return ((r - this.r) * (r - this.r) + (g - this.g) * (g - this.g) + (b - this.b) * (b - this.b)) / 3;
    }

    public static Color of(String name, int r, int g, int b) {
        return new Color(name, r, g, b);
    }

    public static Color of(int r, int g, int b) {
        return Arrays.stream(KNOWN_COLORS)
                .reduce(BinaryOperator.minBy(MeanSquaredDeviationComparator.of(r, g, b)))
                .orElse(null);
    }

    private static class MeanSquaredDeviationComparator implements Comparator<Color> {
        private final int r;
        private final int g;
        private final int b;

        private MeanSquaredDeviationComparator(int r, int g, int b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public static MeanSquaredDeviationComparator of(int r, int g, int b) {
            return new MeanSquaredDeviationComparator(r, g, b);
        }

        @Override
        public int compare(Color color, Color otherColor) {
            return color.mse(r, g, b) - otherColor.mse(r, g, b);
        }
    }
}

