package eu.elision.hybrisvision.hybrisvisionidentificationservice.util;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.EntityAnnotation;
import com.google.cloud.vision.v1.WebDetection;

import java.lang.reflect.Field;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ImageUtil {

    public static final String SAFE_SEARCH_ADULT = "adult";
    public static final String SAFE_SEARCH_SPOOF = "spoof";
    public static final String SAFE_SEARCH_MEDICAL = "medical";
    public static final String SAFE_SEARCH_VIOLENCE = "violence";
    public static final String SAFE_SEARCH_RACY = "racy";
    private static final String CHARACTER_NEW_LINE = "\n";

    public static List<String> getColors(AnnotateImageResponse response) {

        return response
                .getImagePropertiesAnnotation()
                .getDominantColors()
                .getColorsList()
                //.stream().map(colorInfo -> colorInfo.getColor().toString())
                .stream().map(colorInfo ->  Color.of((int)colorInfo.getColor().getRed(),
                        (int)colorInfo.getColor().getGreen(), (int)colorInfo.getColor().getBlue()).getName())
                .collect(Collectors.toList());
    }

    public static Map<String, Double> getWebSearch(AnnotateImageResponse response) {

       List<String> strings = new ArrayList<>();
       strings.add("product");
       strings.add("design");
       strings.add("product design");
       strings.add("brand");

        return response
                .getWebDetection()
                .getWebEntitiesList()
                .stream().filter(distinctByKey(WebDetection.WebEntity::getDescription))
                .filter(k -> !k.getDescription().equals(""))
                .filter(k -> !strings.contains(k.getDescription().toLowerCase()))
                .collect(Collectors.toMap(
                        WebDetection.WebEntity::getDescription,
                        e -> ((double)(e.getScore()))));
    }

    public static Map<String, String> extractSafeSearch(AnnotateImageResponse response) {

        Map<String, String> safeSearch = new HashMap<>();
        safeSearch.put(SAFE_SEARCH_ADULT, response.getSafeSearchAnnotation().getAdult().toString());
        safeSearch.put(SAFE_SEARCH_SPOOF, response.getSafeSearchAnnotation().getSpoof().toString());
        safeSearch.put(SAFE_SEARCH_MEDICAL, response.getSafeSearchAnnotation().getMedical().toString());
        safeSearch.put(SAFE_SEARCH_VIOLENCE, response.getSafeSearchAnnotation().getViolence().toString());
        safeSearch.put(SAFE_SEARCH_RACY, response.getSafeSearchAnnotation().getRacy().toString());
        return safeSearch;
    }

    public static List<String> extractText(AnnotateImageResponse response) {

        return response
                .getTextAnnotationsList()
                .stream().map(EntityAnnotation::getDescription)
                .filter(description -> !description.contains(CHARACTER_NEW_LINE))
                .collect(Collectors.toList());
    }

    public static Map<String, Double> extractLabels(AnnotateImageResponse response) {

        return response
                .getLabelAnnotationsList()
                .stream().filter(distinctByKey(EntityAnnotation::getDescription))
                .collect(Collectors.toMap(
                        EntityAnnotation::getDescription,
                        e -> ((double)(e.getScore()))));
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static List<String> getLogos(AnnotateImageResponse response) {

        return response
                .getLogoAnnotationsList()
                .stream().map(EntityAnnotation::getDescription)
                .collect(Collectors.toList());
    }

    public static void mapLabeltoColor(Map<String, Double> labels, List<String> colors) {
        Iterator it = labels.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Color color;
            try {
                String fieldName = pair.getKey().toString().toUpperCase();
                if(fieldName.equalsIgnoreCase("brown")) {
                    colors.add(pair.getKey().toString());
                    it.remove();
                }
                Field field = Class.forName("java.awt.Color").getField(fieldName);
                if(field != null) {
                    colors.add(pair.getKey().toString());
                    it.remove();
                }
            } catch (Exception e) {
                color = null; // Not defined
            }
        }
    }

}
