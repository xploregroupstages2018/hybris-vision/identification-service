package eu.elision.hybrisvision.hybrisvisionidentificationservice.model;

import java.util.List;
import java.util.Map;

/**
 * @author Lander
 * @since 24/04/2018
 */
public class Response {

    private List<String> logos;
    private Map<String, Double> labels;
    private List<String> textList;
    private Map<String, String> safeSearch;
    private List<String> colors;
    private Map<String, Double> webSearch;

    public static class ResponseBuilder {

        private List<String> logos;
        private Map<String, Double> labels;
        private List<String> textList;
        private Map<String, String> safeSearch;
        private List<String> colors;
        private Map<String, Double> webSearch;

        public ResponseBuilder withLogos(List<String> logos) {
            this.logos = logos;
            return this;  //By returning the builder each time, we can create a fluent interface.
        }

        public ResponseBuilder withLabels(Map<String, Double> labels) {
            this.labels = labels;
            return this;
        }

        public ResponseBuilder withText(List<String> text) {
            this.textList = text;
            return this;
        }

        public ResponseBuilder withSafeSearch(Map<String, String> safeSearch) {
            this.safeSearch = safeSearch;
            return this;
        }

        public ResponseBuilder withColors(List<String> colors) {
            this.colors = colors;
            return this;
        }

        public ResponseBuilder withWeb(Map<String, Double> webSearch) {
            this.webSearch = webSearch;
            return this;
        }

        public Response build() {
            //Here we create the actual response object, which is always in a fully initialised state when it's returned.
            Response response = new Response();  //Since the builder is in the Response class, we can invoke its private constructor.
            response.setLogos(logos);
            response.setLabels(labels);
            response.setSafeSearch(safeSearch);
            response.setTextList(textList);
            response.setColors(colors);
            response.setWebSearch(webSearch);
            return response;
        }
    }

    private Response() {
    }

    public List<String> getLogos() {
        return logos;
    }

    public void setLogos(List<String> logos) {
        this.logos = logos;
    }

    public Map<String, Double> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, Double> labels) {
        this.labels = labels;
    }

    public List<String> getTextList() {
        return textList;
    }

    public void setTextList(List<String> textList) {
        this.textList = textList;
    }

    public Map<String, String> getSafeSearch() {
        return safeSearch;
    }

    public void setSafeSearch(Map<String, String> safeSearch) {
        this.safeSearch = safeSearch;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public Map<String, Double> getWebSearch() {
        return webSearch;
    }

    public void setWebSearch(Map<String, Double> webSearch) {
        this.webSearch = webSearch;
    }
}
