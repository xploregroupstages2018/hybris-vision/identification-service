package eu.elision.hybrisvision.hybrisvisionidentificationservice.controller;

import eu.elision.hybrisvision.hybrisvisionidentificationservice.facade.IdentificationFacade;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Lander
 * @since 24/04/2018
 */
@RestController
@CrossOrigin
public class ImageController {

    @Autowired
    private IdentificationFacade identificationFacade;

    @PostMapping(value = "/image", produces = {"application/json"})
    public ResponseEntity<Response> postImage(@RequestParam MultipartFile file) {
        Response response = identificationFacade.identifyImage(file);
        return ResponseEntity.ok().body(response);
    }
}