package eu.elision.hybrisvision.hybrisvisionidentificationservice.facade;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.model.Response;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class IdentificationFacadeImpl implements IdentificationFacade {

    @Autowired
    private ImageService imageService;

    @Override
    public Response identifyImage(MultipartFile multipartFile) {

        byte[] bytes = imageService.convertToBytes(multipartFile);
        AnnotateImageResponse annotateImageResponse = imageService.analyseImage(bytes);
        return imageService.getLabels(annotateImageResponse);
    }
}
