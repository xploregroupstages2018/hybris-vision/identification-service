package eu.elision.hybrisvision.hybrisvisionidentificationservice.facade;

import eu.elision.hybrisvision.hybrisvisionidentificationservice.model.Response;
import org.springframework.web.multipart.MultipartFile;

public interface IdentificationFacade {
    Response identifyImage(MultipartFile multipartFile);
}
