package eu.elision.hybrisvision.hybrisvisionidentificationservice.exception;

public class ErrorConstant {

    public static final String DEFAULT = "Something went wrong while processing the image";
    public static final String EMPTY_FILE = "The file received is empty";
    public static final String IMAGE_CONVERSION = "Error while converting the image";
    public static final String VISION_EMPTY_RESULT = "No processed images returned by Google";
    public static final String VISION_FAULTY_IMAGE = "Image returned by Google has an error";
    public static final String VISION_CLIENT_CREATION = "Error while creating the Google Cloud Vision API client";
}