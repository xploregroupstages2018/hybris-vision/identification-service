package eu.elision.hybrisvision.hybrisvisionidentificationservice.exception;

public class ImageProcessingException extends RuntimeException {

    public ImageProcessingException(String message) {
        super(message);
    }
}