package eu.elision.hybrisvision.hybrisvisionidentificationservice.service;


import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.protobuf.ByteString;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.exception.ErrorConstant;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.exception.ImageProcessingException;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.model.Response;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.util.Color;
import eu.elision.hybrisvision.hybrisvisionidentificationservice.util.ImageUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Lander
 * @since 24/04/2018
 */

@Service
public class ImageService {

    /**
     * @param file
     * @return bytes of the received MultipartFile
     * @throws ImageProcessingException in case the conversion of the image failed
     */
    public byte[] convertToBytes(MultipartFile file) {

        if (file.isEmpty()) {
            throw new ImageProcessingException(ErrorConstant.EMPTY_FILE);
        }

        try {
            return file.getBytes();
        } catch (IOException e) {
            throw new ImageProcessingException(ErrorConstant.IMAGE_CONVERSION);
        }
    }

    /**
     * @param annotateImageResponse
     * @return Visual information linked to the file
     */
    public Response getLabels(AnnotateImageResponse annotateImageResponse) {

        if (annotateImageResponse != null) {

            Map<String, Double> labels = ImageUtil.extractLabels(annotateImageResponse);
            List<String> textList = ImageUtil.extractText(annotateImageResponse);
            List<String> logos = ImageUtil.getLogos(annotateImageResponse);
            Map<String, Double> webSearch = ImageUtil.getWebSearch(annotateImageResponse);
            List<String> colors = ImageUtil.getColors(annotateImageResponse);
            Map<String, String> safeSearch = ImageUtil.extractSafeSearch(annotateImageResponse);

            ImageUtil.mapLabeltoColor(webSearch, colors);

            colors = colors.stream().distinct().collect(Collectors.toList());

            return new Response.ResponseBuilder()
                    .withLogos(logos)
                    .withLabels(labels)
                    .withText(textList)
                    .withSafeSearch(safeSearch)
                    .withColors(colors)
                    .withWeb(webSearch)
                    .build();
        }

        return null;
    }

    /**
     * @param base64
     * @return Google Vision API processed image. We send an image
     * one at a time so we are only interested in the first response in the list.
     * @throws ImageProcessingException in case the response from Google is empty or contained an error.
     */
    public AnnotateImageResponse analyseImage(byte[] base64) {

        try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {

            byte[] imgByte = Base64.getEncoder().encode(base64);
            imgByte = Base64.getDecoder().decode(imgByte);
            ByteString imgBytes = ByteString.copyFrom(imgByte);

            // Builds the image annotation request
            List<AnnotateImageRequest> requests = new ArrayList<>();
            Image img = Image.newBuilder().setContent(imgBytes).build();
            Feature feat = Feature.newBuilder().setType(Feature.Type.LABEL_DETECTION).setMaxResults(6).build();
            Feature feat2 = Feature.newBuilder().setType(Feature.Type.LOGO_DETECTION).setMaxResults(6).build();
            Feature feat3 = Feature.newBuilder().setType(Feature.Type.SAFE_SEARCH_DETECTION).build();
            Feature feat4 = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build();
            Feature feat5 = Feature.newBuilder().setType(Feature.Type.IMAGE_PROPERTIES).build();
            Feature feat6 = Feature.newBuilder().setType(Feature.Type.WEB_DETECTION).setMaxResults(6).build();
            AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
                    .addFeatures(feat).addFeatures(feat2).addFeatures(feat3).addFeatures(feat4)
                    .addFeatures(feat5).addFeatures(feat6)
                    .setImage(img)
                    .build();
            requests.add(request);

            // Performs label detection on the image file
            BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
            List<AnnotateImageResponse> responsesList = response.getResponsesList();

            if (responsesList.isEmpty()) {
                throw new ImageProcessingException(ErrorConstant.VISION_EMPTY_RESULT);
            } else {

                AnnotateImageResponse annotateImageResponse = response.getResponsesList().get(0);
                if (annotateImageResponse.hasError()) {
                    throw new ImageProcessingException(ErrorConstant.VISION_FAULTY_IMAGE);
                }
                return annotateImageResponse;
            }

        } catch (IOException e) {
            throw new ImageProcessingException(ErrorConstant.VISION_CLIENT_CREATION);
        }
    }
}

