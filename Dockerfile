FROM frolvlad/alpine-oraclejdk8
VOLUME /tmp
ADD /build/libs/hybrisvision-identification-service-?.?.?-SNAPSHOT.jar identification-service.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/identification-service.jar"]